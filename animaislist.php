<?php
include_once 'autentica.php';
include_once 'Classes/Animais.php';

$Animais = new Animais();
$tabAnimais = $Animais->getAll();
if (isset($_GET['q'])) {
    $tabAnimais = $Animais->getByNome($_GET['q']);
} else {
    $tabAnimais = $Animais->getAll();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Animais</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <?php
        include 'menu.php';
        ?>
        <div class="container">
            <form method="get"
                  class="form-inline"
                  style="margin-bottom: 10px">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Pesquisar" name="q" value="<?= isset($_GET['q']) ? $_GET['q'] : '' ?>">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
                <a href="animaisform.php?op=new"
                   class="btn btn-success">
                    <i class="glyphicon glyphicon-plus"></i>
                    Novo Animal
                </a>

                <a href="http://localhost/zoo/relatorio/relatorio.php"
                   class="btn btn-success">
                    Gerar PDF
                </a>
            </form>

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Código</th>
                        <th class="text-center">Nome</th>
                        <th class="text-center">Idade</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($tabAnimais as $linha) {
                        ?>
                        <tr>
                            <td class="text-center col-sm-1"><?= $linha['idAnimal'] ?></td>
                            <td class="col-sm-5">
                                <a href="animaisform.php?op=edit&idAnimal=<?= $linha['idAnimal'] ?>">
                                    <?= $linha['nome'] ?>
                                </a>
                            </td>
                            <td class="text-right col-sm-2"><?= $linha['idade'] ?></td>
                            <td class="text-center col-sm-2">
                                <a href="animaisform.php?op=edit&idAnimal=<?= $linha['idAnimal'] ?>">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                &nbsp;&nbsp;
    <!--                                <a href="animaisform.php?op=delete&id=<?= $linha['idAnimal'] ?>" class="lixeira" onclick="return confirm('Confirma a exclusão?')">-->
                                <a href="animaisform.php?op=delete&idAnimal=<?= $linha['idAnimal'] ?>" class="lixeira">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
            if (count($tabAnimais) == 0) {
                ?>
                <div class="alert alert-info">
                    Nenhum animal encontrado!
                </div>
                <?php
            }
            ?>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.lixeira').on('click', function() {
                    return confirm('Confirma a exclusão?');
                })
            })
        </script>
    </body>
</html>
