<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Zoológico</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="animaislist.php">Animais</a></li>
            <li><a href="especielist.php">Espécie</a></li>
            <li><a href="generolist.php">Gênero</a></li>
            <li><a href="locallist.php">Local do Zoológico</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php
            @session_start();
            if (!isset($_SESSION['login'])) {
                ?>
                <li><a href="registrar.php">
                        <span class="glyphicon glyphicon-user"></span>
                        Registrar
                    </a>
                </li>
                <li><a href="login.php">
                        <span class="glyphicon glyphicon-log-in"></span>
                        Login
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li><a href="logout.php">
                        <span class="glyphicon glyphicon-log-out"></span>
                        Sair
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</nav>
