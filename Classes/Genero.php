<?php

require_once 'DAO.php';

class Genero extends DAO {

    public function getAll(){
        $query = parent::$db->prepare("SELECT *"
                . " FROM genero"
                . " ORDER BY Nome");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    public function insert($dados) {
        $query = parent::$db->prepare(
                "INSERT INTO genero "
                . " (nome) "
                . " VALUES "
                . " (:nome)");
        return $query->execute($dados);
    }
     public function update($dados) {
        $query = parent::$db->prepare(
                "UPDATE genero"
                . " SET nome=:nome"
                . " WHERE idGenero=:idGenero");
        return $query->execute($dados);
    }


    public function delete($id) {
        $query = parent::$db->prepare(
                "DELETE FROM genero "
                . " WHERE idGenero =:idGenero");
        return $query->execute(["idGenero" => $id]);
    }
    public function getById($id) {
        $query = parent::$db->prepare(
                "SELECT * FROM genero "
                . "WHERE idGenero=:idGenero");
        $query->execute(['idGenero' => $id]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }
     public function getByNome($q) {
        $query = parent::$db->prepare(
                'SELECT * '
                . ' FROM genero '
                . ' WHERE Nome LIKE :q'
                . ' ORDER BY Nome');

        $query->execute(['q' => '%' . $q . '%']);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

}
