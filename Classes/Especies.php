<?php

require_once 'DAO.php';

class Especies extends DAO{

    public function getAll(){
        $query = parent::$db->prepare("SELECT *"
                . " FROM especie"
                . " ORDER BY nome");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert($dados) {
        $query = parent::$db->prepare(
                "INSERT INTO especie "
                . " (nome) "
                . " VALUES "
                . " (:nome)");
        return $query->execute($dados);
    }

    public function update($dados) {
        $query = parent::$db->prepare(
                "UPDATE especie SET "
                . " Nome=:Nome "
                . " WHERE idEspecie=:idEspecie");
        return $query->execute($dados);
    }

    public function delete($id) {
        $query = parent::$db->prepare(
                "DELETE FROM especie "
                . " WHERE idEspecie=:idEspecie");
        return $query->execute(["idEspecie" => $id]);
    }

     public function getById($id) {
        $query = parent::$db->prepare(
                "SELECT * FROM especie "
                . "WHERE idEspecie=:idEspecie");
        $query->execute(['idEspecie' => $id]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function getByNome($q) {
        $query = parent::$db->prepare(
                'SELECT * '
                . ' FROM especie '
                . ' WHERE Nome LIKE :q'
                . ' ORDER BY Nome');

        $query->execute(['q' => '%' . $q . '%']);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
