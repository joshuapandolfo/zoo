<?php

include_once 'DAO.php';

class Animais extends DAO {

    public function getAll() {
        $query = parent::$db->prepare('SELECT idAnimal, nome, idade FROM animais ORDER BY nome');
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getByNome($q) {
        $query = parent::$db->prepare(
                'SELECT * '
                . ' FROM animais '
                . ' WHERE nome LIKE :q'
                . ' ORDER BY nome');

        $query->execute(['q' => '%' . $q . '%']);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById($id) {
        $query = parent::$db->prepare(
                "SELECT idAnimal, nome, idade FROM animais "
                . "WHERE idAnimal=:idAnimal");
        $query->execute(['idAnimal' => $id]);
        return $query->fetch(PDO::FETCH_ASSOC);
        $lista = $query->fetchAll();
    }

    public function insert($dados) {
        $query = parent::$db->prepare(
                "INSERT INTO animais "
                . " (nome, idade, idEspecie, idGenero, idLocal) "
                . " VALUES "
                . " (:nome, :idade, :idEspecie, :idGenero, :idLocal)");
                 //print_r($dados);
        return $query->execute($dados);
    }

    public function update($dados) {
        $query = parent::$db->prepare(
                "UPDATE animais "
                . " SET nome=:nome, idade=:idade, idEspecie=:idEspecie, idGenero=:idGenero, idLocal=:idLocal"
                . " WHERE idAnimal=:idAnimal");
        return $query->execute($dados);
    }

    public function delete($id) {
        $query = parent::$db->prepare(
                "DELETE FROM animais "
                . " WHERE idAnimal=:idAnimal");
        return $query->execute(["idAnimal" => $id]);
    }
}
