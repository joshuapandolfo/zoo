<?php
include_once 'DAO.php';

class LocalZoo extends DAO{
    public function getAll(){
        $query = parent::$db->prepare("SELECT *"
                . " FROM localzoo"
                . " ORDER BY nome");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    public function insert($dados) {
        $query = parent::$db->prepare(
                "INSERT INTO localzoo "
                . " (nome) "
                . " VALUES "
                . " (:nome)");
        return $query->execute($dados);
    }

    public function update($dados) {
        $query = parent::$db->prepare(
                "UPDATE localzoo SET "
                . " nome=:nome "
                . " WHERE idLocal=:idLocal");
        return $query->execute($dados);
    }

    public function delete($id) {
        $query = parent::$db->prepare(
                "DELETE FROM localzoo "
                . " WHERE idlocal=:idlocal");
        return $query->execute(["idlocal" => $id]);
    }
     public function getById($id) {
        $query = parent::$db->prepare(
                "SELECT * FROM localzoo "
                . "WHERE idlocal=:idlocal");
        $query->execute(['idlocal' => $id]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }
    public function getByNome($q) {
        $query = parent::$db->prepare(
                'SELECT * '
                . ' FROM localzoo '
                . ' WHERE nome LIKE :q'
                . ' ORDER BY nome');

        $query->execute(['q' => '%' . $q . '%']);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
