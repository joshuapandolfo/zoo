<?php

include_once 'DAO.php';

class Usuarios extends DAO {

    public function getByLogin($login){
        $query = parent::$db->prepare('SELECT * '
                . ' FROM usuarios '
                . ' WHERE login=:login');

        $query->execute(['login'=>$login]);
        return $query->fetch();
    }

    public function insert($dados){
        $query = parent::$db->prepare("INSERT INTO usuarios "
                . " (login, senha) "
                . " VALUES (:login, :senha)");

        return $query->execute($dados);
    }
}
