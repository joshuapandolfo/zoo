<?php
  require_once('../Classes/Animais.php');

  //tabela

	$html = '<table border=2';
	$html .= '<thead>';
	$html .= '<tr>';
	$html .= '<th>ID ANIMAL</th>';
	$html .= '<th>NOME</th>';
	$html .= '<th>IDADE DO ANIMAL</th>';

  $Animais = new Animais();

  $tabAnimais = $Animais->getAll();



  foreach ($tabAnimais as $d) {
    $html .= '<tr><td>'.$d['idAnimal'] . "</td>";
		$html .= '<td>'.$d['nome'] . "</td>";
		$html .= '<td>'.$d['idade'] . "</td>";
  }

	$html .= '</tbody>';
	$html .= '</table';


	//referenciar o DomPDF com namespace
	use Dompdf\Dompdf;

	// include autoloader
	require_once("../dompdf/autoload.inc.php");

	//Criando a Instancia
	$dompdf = new DOMPDF();

	// Carrega seu HTML
	$dompdf->load_html('
			<h1 style="text-align: center;">RELATÓRIO DOS ANIMAIS</h1>
			'. $html .'
		');

	//Renderizar o html
	$dompdf->render();

	//Exibibir a página
	$dompdf->stream(
		"relatorio_animais.pdf",
		array(
			"Attachment" => false //Colocar true para fazer download
		)
	);
