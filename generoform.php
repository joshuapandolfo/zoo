<?php
include_once 'autentica.php';
require_once 'Classes/Genero.php';

$Genero = new Genero();

$op = isset($_GET['op']) ? $_GET['op'] : '';
if (isset($_POST['salvar'])) {
    // Validações server side
}
if ($op == 'new' && isset($_POST['salvar'])) {
    echo 'if new';
    unset($_POST['idGenero'], $_POST['salvar']);

    if ($Genero->insert(["nome" => $_POST['nome']])) {
        header("Location: generolist.php");
        exit();
    }
} elseif ($op == "edit" && isset($_GET['idGenero'])) {
    if (isset($_POST['salvar'])) {
        unset($_POST['salvar']);
        if ($Genero->update ($_POST)) {
            header("Location: generolist.php");
            exit();
        } else {
            // mostra mensagem
            $prod = $_POST;
        }
    } else {
        $prod = $Genero->getById($_GET['idGenero']);
    }
} elseif ($op == "delete" && isset($_GET['idGenero'])) {
    $Genero->delete($_GET['idGenero']);
    header("Location: generolist.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Novo Gênero</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'menu.php'; ?>
        <div class="container">
            <div class="row">
                <h2 class="col-sm-10 col-sm-offset-2">Novo Gênero</h2>
            </div>
            <form method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2">
                        Código:
                    </label>
                    <div class="col-sm-10">
                        <input type="text"
                               class="form-control"
                               name="idGenero"
                               readonly=""
                               value="<?= isset($prod) ? $prod['idGenero'] : '' ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">
                        Nome:
                    </label>
                    <div class="col-sm-10">
                        <input type="text"
                               class="form-control"
                               name="nome"
                               required=""
                               autofocus=""
                               value="<?= isset($prod) ? $prod['nome'] : '' ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button type="submit" name="salvar" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok"></span>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
