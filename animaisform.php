<?php

include_once 'autentica.php';
require_once 'Classes/Animais.php';
require_once 'Classes/Especies.php';
require_once 'Classes/Genero.php';
require_once 'Classes/LocalZoo.php';

$Animais = new Animais();
$Especies = new Especies();
$Genero = new Genero();
$LocalZoo = new LocalZoo();

$tabespecie = $Especies->getAll();
$tabgenero = $Genero->getAll();
$tabLocalZoo = $LocalZoo->getAll();

$op = isset($_GET['op']) ? $_GET['op'] : '';
if (isset($_POST['salvar'])) {
    // Validações server side
}
if ($op == 'new' && isset($_POST['salvar'])) {
  // echo 'if new';
    unset($_POST['idAnimal'], $_POST['salvar']);

    if ($Animais->insert($_POST)){
        header("Location: animaislist.php");
        exit();
    }

} elseif ($op == "edit" && isset($_GET['idAnimal'])) {
    if (isset($_POST['salvar'])) {
        unset($_POST['salvar']);

        if ($Animais->update($_POST)) {
            header("Location: animaislist.php");
            exit();
        } else {
            // mostra mensagem
            $prod = $_POST;
        }
    } else {
        $prod = $Animais->getById($_GET['idAnimal']);
    }
} elseif ($op == "delete" && isset($_GET['idAnimal'])) {
    $Animais->delete($_GET['idAnimal']);
    header("Location: animaislist.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Novo Animal</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'menu.php'; ?>
        <div class="container">
            <div class="row">
                <h2 class="col-sm-10 col-sm-offset-2">Novo Animal</h2>
            </div>
            <form method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2">
                        Código:
                    </label>
                    <div class="col-sm-10">
                        <input type="text"
                               class="form-control"
                               name="idAnimal"
                               readonly=""
                               value="<?= isset($prod) ? $prod['idAnimal'] : '' ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">
                        Nome:
                    </label>
                    <div class="col-sm-10">
                        <input type="text"
                               class="form-control"
                               name="nome"
                               required=""
                               autofocus=""
                               value="<?= isset($prod) ? $prod['nome'] : '' ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">
                        Idade:
                    </label>
                    <div class="col-sm-10">
                        <input type="number"
                               class="form-control"
                               name="idade"
                               required=""
                               min="0"
                               value="<?= isset($prod) ? $prod['idade'] : '' ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">
                        Espécie:
                    </label>
                    <div class="col-sm-10">
                        <select name="idEspecie"
                                required=""
                                class="form-control">
                            <option value="">
                                Selecione a Espécie!
                            </option>
                            <?php
                            foreach ($tabespecie as $cat) {
                                $opcao = isset($prod) &&
                                        $cat['idEspecie'] == $prod['idEspecie'] ? 'selected' : '';
                                ?>
                                <option value="<?= $cat['idEspecie'] ?>" <?= $opcao ?> >
                                    <?= $cat['nome'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">
                        Gênero:
                    </label>
                    <div class="col-sm-10">
                        <select name="idGenero"
                                required=""
                                class="form-control">
                            <option value="">
                                Selecione o Gênero!
                            </option>
                            <?php
                            foreach ($tabgenero as $cat) {
                                $opcao = isset($prod) &&
                                        $cat['idGenero'] == $prod['idGenero'] ? 'selected' : '';
                                ?>
                                <option value="<?= $cat['idGenero'] ?>" <?= $opcao ?> >
                                    <?= $cat['nome'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">
                        Local Zoo:
                    </label>
                    <div class="col-sm-10">
                        <select name="idLocal"
                                required=""
                                class="form-control">
                            <option value="">
                                Selecione o local!
                            </option>
                            <?php
                            foreach ($tabLocalZoo as $cat) {
                                $opcao = isset($prod) &&
                                        $cat['idLocal'] == $prod['idLocal'] ? 'selected' : '';
                                ?>
                                <option value="<?= $cat['idLocal'] ?>" <?= $opcao ?> >
                                    <?= $cat['nome'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button type="submit" name="salvar" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok"></span>
                            Salvar
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
