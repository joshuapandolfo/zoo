-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Jun-2019 às 00:55
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trab_ads_132`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `animais`
--

CREATE TABLE `animais` (
  `idAnimal` int(11) NOT NULL,
  `nome` text NOT NULL,
  `idade` int(11) NOT NULL,
  `idEspecie` int(11) NOT NULL,
  `idGenero` int(11) NOT NULL,
  `idLocal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `animais`
--

INSERT INTO `animais` (`idAnimal`, `nome`, `idade`, `idEspecie`, `idGenero`, `idLocal`) VALUES
(4, 'LEÃO', 4, 10, 7, 3),
(5, 'kkkkkkkk', 5, 12, 7, 7),
(6, 'Vaca', 20, 9, 4, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `especie`
--

CREATE TABLE `especie` (
  `idEspecie` int(11) NOT NULL,
  `nome` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `especie`
--

INSERT INTO `especie` (`idEspecie`, `nome`) VALUES
(3, 'Mamífero'),
(4, 'Anfíbio'),
(5, 'Réptil'),
(6, 'Joshuashuashuashua'),
(7, 'haue'),
(8, 'jiaeuaheauehaueha'),
(9, 'Mula'),
(10, 'hahaha'),
(11, 'laoaoa'),
(12, 'faada'),
(13, 'pamela');

-- --------------------------------------------------------

--
-- Estrutura da tabela `genero`
--

CREATE TABLE `genero` (
  `idGenero` int(11) NOT NULL,
  `nome` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `genero`
--

INSERT INTO `genero` (`idGenero`, `nome`) VALUES
(2, 'Macho'),
(4, 'Fêmea'),
(5, 'Parte pros 2 lados'),
(6, 'Hermafrodita'),
(7, 'eaheuahea'),
(8, 'kk'),
(9, 'kkkkkk'),
(10, 'kdmkdm'),
(11, 'adada'),
(12, 'alguma coisa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `localzoo`
--

CREATE TABLE `localzoo` (
  `idLocal` int(11) NOT NULL,
  `nome` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `localzoo`
--

INSERT INTO `localzoo` (`idLocal`, `nome`) VALUES
(3, 'Marau'),
(4, 'Passo Fundo City'),
(5, 'Serafina Corrêa'),
(6, 'UPF PARQUE'),
(7, 'Motel'),
(8, 'um lugar por aí'),
(9, 'dadasasa'),
(10, 'youtube');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(25) NOT NULL,
  `login` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `login`, `senha`) VALUES
(1, 'admin', '$2y$10$DXo.L8qq7YBeGK7TsO7NBOnKNAa27lN9CDwhtMhSV3pWeQgSqfIgG'),
(3, 'joshuapandolfo', '$2y$10$p7hWZ/nXlzhYvWUo.MsKLunFr8n1ZResr62aTyGUnXGEQULD4VNUS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animais`
--
ALTER TABLE `animais`
  ADD PRIMARY KEY (`idAnimal`),
  ADD KEY `idEspecie` (`idEspecie`),
  ADD KEY `idGenero` (`idGenero`),
  ADD KEY `idLocal` (`idLocal`);

--
-- Indexes for table `especie`
--
ALTER TABLE `especie`
  ADD PRIMARY KEY (`idEspecie`);

--
-- Indexes for table `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`idGenero`);

--
-- Indexes for table `localzoo`
--
ALTER TABLE `localzoo`
  ADD PRIMARY KEY (`idLocal`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animais`
--
ALTER TABLE `animais`
  MODIFY `idAnimal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `especie`
--
ALTER TABLE `especie`
  MODIFY `idEspecie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `genero`
--
ALTER TABLE `genero`
  MODIFY `idGenero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `localzoo`
--
ALTER TABLE `localzoo`
  MODIFY `idLocal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `animais`
--
ALTER TABLE `animais`
  ADD CONSTRAINT `animais_ibfk_1` FOREIGN KEY (`idEspecie`) REFERENCES `especie` (`idEspecie`),
  ADD CONSTRAINT `animais_ibfk_2` FOREIGN KEY (`idGenero`) REFERENCES `genero` (`idGenero`),
  ADD CONSTRAINT `animais_ibfk_3` FOREIGN KEY (`idLocal`) REFERENCES `localzoo` (`idLocal`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
