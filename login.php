<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <?php
        include 'menu.php';
        include 'Classes/Usuarios.php';

        if (isset($_POST['entrar'])) {
            $usuarios = new Usuarios();
            $u = $usuarios->getByLogin($_POST['login']);
            if ($u && password_verify($_POST['senha'], $u['senha'])) {
                @session_start();
                $_SESSION['login'] = $_POST['login'];
                header('Location: animaislist.php');
                exit();
            } else {
                echo 'Login ou senha inválido';
            }
        }
        ?>

        <!-- Template from https://bootsnipp.com/snippets/featured/login-amp-signup-forms-in-panel -->
        <div style="margin-top:50px;" class="container mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Login</div>
                </div>

                <div style="padding-top:30px" class="panel-body">

                    <form class="form-horizontal" method="post">

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="login" placeholder="Informe seu login" class="form-control" required="" autofocus="">
                        </div>

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" name="senha" placeholder="Informe sua senha" required="" class="form-control">
                        </div>

                        <div style="margin-top:10px" class="form-group">
                            <div class="col-sm-12 controls">
                                <button type="submit" name="entrar" class="btn btn-primary">
                                    Entrar
                                </button>
                                <a href="registrar.php" class="btn btn-link">
                                    Criar uma conta
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="js/bootstrap.min.js"></script>
    </body>
</html>
